﻿(function() {
    "use strict";

    angular
        .module("fundraisingApp")
        .factory("SetupService", SetupService);

    SetupService.$inject = ["$http"];

    function SetupService($http) {

        var service = {
            addDonation: addDonation
        };

        //Create donation
        function addDonation(donationData) {
            return $http.post("/FundRaising/PostDonation", donationData)
                .then(function(response) {
                    return response.data;
                })
                .catch(function() {
                    console.log("Error while saving donation!");
                });
        };

        return service;
    }
})();