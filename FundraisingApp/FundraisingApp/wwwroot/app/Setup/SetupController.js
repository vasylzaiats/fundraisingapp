﻿(function() {
    "use strict";

    angular.module("fundraisingApp")
        .controller("SetupController", SetupController);

    SetupController.$inject = ["SetupService", "$scope", "$http", "toastr", "$window"];

    function SetupController(setupService, $scope, $http, toastr, $window) {
        var vm = this;

        vm.saveDonation = saveDonation;

        $window.document.title = "Setup - Fundraising";

        vm.donation = {};

        activate();

        function activate() {
            vm.calendarStartDate = {
                isOpen: false,
                openCalendarStart: openCalendarStart,
                timepickerOptions: {
                    showMeridian: false
                }
            };

            vm.calendarEndDate = {
                isOpen: false,
                openCalendarEnd: openCalendarEnd,
                timepickerOptions: {
                    showMeridian: false
                }
            };
        }

        //Save Donation
        function saveDonation() {
            setupService.addDonation(vm.donation)
                .then(function() {
                    toastr.success(
                        "Thank you for donating!",
                        "Saved",
                        {
                            closeButton: true,
                            timeOut: 5000
                        });
                });
        }

        function openCalendarStart(e) {
            e.preventDefault();
            e.stopPropagation();

            vm.calendarStartDate.isOpen = true;
        };

        function openCalendarEnd(e) {
            e.preventDefault();
            e.stopPropagation();

            vm.calendarEndDate.isOpen = true;
        };
    }
}());