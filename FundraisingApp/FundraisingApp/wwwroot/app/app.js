﻿angular.module("fundraisingApp",
        [
            "ui.router", "toastr", "angular-loading-bar", "ngAnimate", "ui.bootstrap", "ui.bootstrap.datetimepicker"
        ])
    .config([
        "$stateProvider", "$urlRouterProvider",
        function($stateProvider, $urlRouterProvider) {

            $stateProvider
                .state("Setup", // Setup page
                    {
                        url: "/",
                        templateUrl: "/App/Setup/Setup.html",
                        controller: "SetupController",
                        controllerAs: "setupCtrl"
                    });

            //This is when any route not matched
            $urlRouterProvider.otherwise("/");
        }
    ]);