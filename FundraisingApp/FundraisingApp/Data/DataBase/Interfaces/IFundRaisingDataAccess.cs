﻿using System.Collections.Generic;

namespace FundraisingApp.Data.DataBase.Interfaces
{
    public interface IFundRaisingDataAccess
    {
        IEnumerable<T> Query<T>(string sql);
        int Execute(string sql, object param);
    }
}
