﻿using System.Collections.Generic;
using System.Data;
using Dapper;
using FundraisingApp.Data.DataBase.Interfaces;

namespace FundraisingApp.Data.DataBase
{
    public class FundRaisingDataAccess : IFundRaisingDataAccess
    {
        private readonly IFundRaisingContext _context;

        public FundRaisingDataAccess(IFundRaisingContext context)
        {
            _context = context;
        }

        public IEnumerable<T> Query<T>(string sql)
        {
                return _context.Db.Query<T>(sql);   
        }

        public int Execute(string sql, object param)
        {
                return _context.Db.Execute(sql, param, commandType: CommandType.StoredProcedure);
            
        }
    }
}