﻿using System;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Configuration;

namespace FundraisingApp.Data
{
    public class FundRaisingContext : IDisposable, IFundRaisingContext
    {
        private string ConnectionString { get; set; }
        private readonly IDbConnection _db;

        public FundRaisingContext(IConfiguration configuration)
        {
            ConnectionString = configuration.GetValue<string>("FundraisingDb:ConnectionString");
            _db = new SqlConnection(ConnectionString);
        }

        public IDbConnection GetOpenConnection(string connectionString)
        {
            try
            {
                    _db.Open();
                    return _db;
            }
            catch (Exception ex)
            {
                var error = new Exception("Error open connection!", ex);
                throw error;
            }
        }

        public IDbConnection Db => _db ?? GetOpenConnection(ConnectionString);

        public void Dispose()
        {
            if (_db != null)
            {
                try
                {
                    if (_db.State != ConnectionState.Closed)
                        _db.Close();
                }
                finally
                {
                    _db.Dispose();
                }
            }
            GC.SuppressFinalize(this);
        }
    }
}
