﻿using System.Data;

namespace FundraisingApp.Data
{
    public interface IFundRaisingContext
    {
        IDbConnection Db { get; }
        IDbConnection GetOpenConnection(string connectionString);
    }
}