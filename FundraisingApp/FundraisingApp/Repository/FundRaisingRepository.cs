﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using FundraisingApp.Data.DataBase.Interfaces;
using FundraisingApp.Model;
using FundraisingApp.Repository.Interfaces;

namespace FundraisingApp.Repository
{
    public class FundRaisingRepository : IFundRaisingRepository
    {
        private readonly IFundRaisingDataAccess _dataAccess;

        public FundRaisingRepository(IFundRaisingDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        //Add new donation
        public void AddNewDonation(FundRaising fundRaising)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@Donation_name", fundRaising.donation_name);
            param.Add("@Start_date", fundRaising.start_date);
            param.Add("@End_date", fundRaising.end_date);
            param.Add("@Amount", fundRaising.amount);
            param.Add("@Level_name", fundRaising.level_name);
            param.Add("@Allow_custom_amounts", fundRaising.allow_custom_amounts);

            _dataAccess.Execute("AddNewDonation", param);
        }

        //Show all donations
        public ICollection<FundRaising> GetAllDonations()
        {
            ICollection<FundRaising> fundRaisingCollection =
                _dataAccess.Query<FundRaising>("GetDonations").ToList();

            return fundRaisingCollection.ToList();
        }

        public void UpdateDonation(FundRaising fundRaising)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@Donation_id", fundRaising.donation_id);
            param.Add("@Donation_name", fundRaising.donation_name);
            param.Add("@Start_date", fundRaising.start_date);
            param.Add("@End_date", fundRaising.end_date);
            param.Add("@Amount", fundRaising.amount);
            param.Add("@Level_name", fundRaising.level_name);
            param.Add("@Allow_custom_amounts", fundRaising.allow_custom_amounts);

            _dataAccess.Execute("UpdateDonation", param);
        }

        public bool DeleteDonation(int id)
        {
            DynamicParameters param = new DynamicParameters();
            param.Add("@Donation_id", id);

            _dataAccess.Execute("DeleteDonationById", param);

            return true;
        }
    }
}