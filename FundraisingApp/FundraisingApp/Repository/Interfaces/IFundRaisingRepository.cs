﻿using System.Collections.Generic;
using FundraisingApp.Model;

namespace FundraisingApp.Repository.Interfaces
{
    public interface IFundRaisingRepository
    {
        void AddNewDonation(FundRaising fundRaising);
        ICollection<FundRaising> GetAllDonations();
        void UpdateDonation(FundRaising fundRaising);
        bool DeleteDonation(int id);
    }
}
