using System.Linq;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using FundraisingApp.Repository.Interfaces;
using FundraisingApp.Model;

namespace FundraisingApp.Controllers
{
    public class FundRaisingController : Controller
    {
        private readonly IFundRaisingRepository _repository;

        public FundRaisingController(IFundRaisingRepository repository)
        {
            _repository = repository;
        }

        // GET: GetAllDonations
        [HttpGet]
        public ActionResult GetAllDonations()
        {
            var result = _repository.GetAllDonations();

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        // POST: FundRaising/PostDonation
        [HttpPost]
        public IActionResult PostDonation([FromBody]FundRaising fundRaising)
        {
            if (fundRaising == null)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.AddNewDonation(fundRaising);

            return CreatedAtRoute("default", new { fundRaising });
        }

        // GET: FundRaising/DonationDetails/5
        [HttpGet]
        public IActionResult DonationDetails(int id)
        {
            var result = _repository.GetAllDonations().FirstOrDefault(d => d.donation_id == id);

            if (result == null)
            {
                return NotFound();
            }

            return Ok(result);
        }

        // POST: FundRaising/PutDonation/5
        [HttpPut]
        [ValidateAntiForgeryToken]
        public IActionResult PutDonation([FromBody]FundRaising fundRaising)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.UpdateDonation(fundRaising);

            return StatusCode((int) HttpStatusCode.NoContent);
        }

        // GET: FundRaising/DeleteDonation/5
        [HttpDelete]
        public IActionResult DeleteDonation(int id)
        {
            _repository.DeleteDonation(id);

            return Ok();
        }
    }
}