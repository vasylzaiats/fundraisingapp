﻿using System;

namespace FundraisingApp.Model
{
    public class FundRaising
    {
        public int donation_id { get; set; }
        public string donation_name { get; set; }
        public DateTime start_date { get; set; }
        public DateTime end_date { get; set; }
        public bool allow_custom_amounts { get; set; }
        public int amount { get; set; }
        public string level_name { get; set; }
        public int fk_donation_id { get; set; }
       
    }
}
