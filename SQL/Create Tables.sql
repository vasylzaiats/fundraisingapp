USE MASTER
GO

IF EXISTS(select * from sys.databases where name='Fundraising_DB')
DROP DATABASE Fundraising_DB

CREATE DATABASE Fundraising_DB;
GO

USE Fundraising_DB
GO

CREATE TABLE DonationCampaign (
donation_id INT IDENTITY(1,1) PRIMARY KEY NOT NULL,
donation_name NVARCHAR(500) NULL,
start_date DATETIME NULL,
end_date DATETIME NULL,
allow_custom_amounts BIT);
GO

CREATE TABLE DonationLevel (
donation_level_id INT IDENTITY(1,1) PRIMARY KEY,
amount INT NULL,
level_name NVARCHAR(500) NULL,
fk_donation_id INT NULL
CONSTRAINT Fk_DonationCampaign_DonationLevel FOREIGN KEY (fk_donation_id) REFERENCES DonationCampaign (donation_id) ON DELETE CASCADE);
GO

INSERT INTO DonationCampaign (donation_name, start_date, end_date, allow_custom_amounts)
VALUES ('Sample Donation 1', GETDATE(), DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), +2), 1),
('Sample Donation 2', GETDATE(), DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), +2), 0),
('Sample Donation 3', GETDATE(), DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), +2), 1),
('Sample Donation 4', GETDATE(), DATEADD(DAY, DATEDIFF(DAY, 0, GETDATE()), +2), 1);
GO

INSERT INTO DonationLevel (amount, level_name, fk_donation_id)
VALUES (50, 'Sample Level name', 1),
(25, 'Sample Level name', 1),
(100, 'Sample Level name', 1),
(50, 'Sample Level name', 2),
(25, 'Sample Level name', 2),
(100, 'Sample Level name', 2),
(100, 'Sample Level name', 3),
(25, 'Sample Level name', 3),
(50, 'Sample Level name', 3),
(100, 'Sample Level name', 4),
(25, 'Sample Level name', 4),
(50, 'Sample Level name', 4);
GO