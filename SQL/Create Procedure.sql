USE Fundraising_DB;
GO

IF (OBJECT_ID('AddNewDonation') IS NOT NULL)
  DROP PROCEDURE AddNewDonation
GO
--Add new donation procedure
Create procedure [dbo].[AddNewDonation]  
(  
    @Donation_name nvarchar(500) NULL, 
	@Start_date NVARCHAR(500) NULL,
	@End_date DATETIME NULL,
	@Allow_custom_amounts BIT,
	@Amount INT NULL,
	@Level_name NVARCHAR(500) NULL)
AS  
BEGIN  
   INSERT INTO dbo.DonationCampaign(donation_name, start_date, end_date, allow_custom_amounts)
		VALUES(@Donation_name, @Start_date, @End_date, @Allow_custom_amounts);  

IF @Amount IS NOT NULL
   INSERT INTO dbo.DonationLevel (amount, level_name, fk_donation_id)
        VALUES (@Amount, @Level_name, SCOPE_IDENTITY());
END
GO


IF (OBJECT_ID('GetDonations') IS NOT NULL)
  DROP PROCEDURE GetDonations
GO
--Get all donations procedure
Create Procedure [dbo].[GetDonations]  
AS  
BEGIN  
	SELECT * FROM DonationCampaign
	LEFT JOIN DonationLevel ON DonationCampaign.donation_id = DonationLevel.fk_donation_id
	ORDER BY DonationCampaign.donation_id;
END
GO

IF (OBJECT_ID('UpdateDonation') IS NOT NULL)
  DROP PROCEDURE UpdateDonation
GO
--Update donation procedure
Create procedure [dbo].[UpdateDonation]  
(  
  @Donation_id INT,  
	@Donation_name nvarchar(500) NULL, 
	@Start_date datetime NULL,
	@End_date datetime NULL,
	@Allow_custom_amounts BIT,
	@Amount int NULL,
	@Level_name nvarchar(500) NULL 
)  
AS  
BEGIN  
   Update DonationCampaign
   set donation_name=@Donation_name,  
   start_date=@Start_date,  
   end_date=@End_date,
   allow_custom_amounts=@Allow_custom_amounts
   where donation_id=@Donation_id  
END 
GO

IF (OBJECT_ID('DeleteDonationById') IS NOT NULL)
  DROP PROCEDURE DeleteDonationById
GO
--Delete donation procedure
Create procedure [dbo].[DeleteDonationById]  
(  
   @Donation_id INT
)  
AS   
BEGIN  
   DELETE FROM dbo.DonationCampaign WHERE donation_id=@Donation_id
END